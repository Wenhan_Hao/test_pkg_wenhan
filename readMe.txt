---------------- Something about Kuka_arm ------------------------
lwr_arm_0_joint: the bottom one



############### KUKA Arm #########################
$ sudo echo
$ roslaunch lmtlwr lmtlwr.launch tool:=gripper

############### Gripper ##########################
$ roslaunch wsg_50_driver wsg_50_udp_script.launch remap:=true

$ rostopic pub /wsg_50_driver/goal_position wsg_50_common/Cmd "mode: ''
pos: 60
speed: 30"

======================================================================================================================
$ rostopic echo /joint_states
----------------initial position---------------------------------------
header: 
  seq: 198622
  stamp: 
    secs: 1526326203
    nsecs: 342468296
  frame_id: ''
name: ['lwr_arm_0_joint', 'lwr_arm_1_joint', 'lwr_arm_2_joint', 'lwr_arm_3_joint', 'lwr_arm_4_joint', 'lwr_arm_5_joint', 'lwr_arm_6_joint']
position: [-0.41655367612838745, 1.3168530464172363, -0.6378539204597473, -1.1116833686828613, -0.7219914197921753, -1.5338581800460815, -0.6272910237312317]
velocity: []
effort: []

-----------------------------------------------------------------------------
$ rosrun test_pkg_wenhan test_fk.py 
[INFO] [WallTime: 1526326193.282690] **test_fk_node initilized..
End-effector postition:  pose_stamped: 
  - 
    header: 
      seq: 0
      stamp: 
        secs: 1526326194
        nsecs: 749118348
      frame_id: base_link
    pose: 
      position: 
        x: -0.559995268072
        y: 0.414966643921
        z: 0.183998922778
      orientation: 
        x: 0.576307864608
        y: -0.394133438673
        z: -0.403908195124
        w: 0.591089035274
fk_link_names: ['lwr_arm_7_link']
error_code: 
  val: 1


=============================================================================================================
$ rostopic echo /wsg_50_driver/status

status: | Fingers Referenced | Target Pos reached |
width: 59.997959137
speed: 0.0
acc: 0.0
force: 0.0
force_finger0: -0.192325592041
force_finger1: 0.21390914917
=========================================================================================================
$ rosservice call /set_gripper_location "pose_stamped:
  header:
    seq: 0
    stamp:
      secs: 0
      nsecs: 0
    frame_id: 'base_link'
  pose:
    position:
      x: -0.56
      y: 0.415
      z: 0.1915
    orientation:
      x: 0.585
      y: -0.40
      z: -0.41
      w: 0.60" 

======================Communication between this 2 computers==========================
*Fist computer(as ros master)
export ROS_IP=10.152.4.76 (own IP address)
export ROS_MASTER_URI=http://10.152.4.76:11311 (This must be the same)

*Second computer
export ROS_IP=10.152.4.76 (own IP address)
export ROS_MASTER_URI=http://10.152.4.76:11311 (This must be the same)

At the beginning, the computer, master computer, must launch the Master node, which can be launched through
a launch file or roscore.


====================== USB Camera ============================================================================
$ roslaunch usb_cam usb_cam-test.launch
$ rosrun image_view image_view image:=/camera/image_raw
$ identify <image_name.xxx>




