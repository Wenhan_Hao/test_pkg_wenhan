#! /usr/bin/env python
# -*- coding: UTF-8 -*-


import roslib; roslib.load_manifest('lmtlwr')
import rospy
import actionlib

from sensor_msgs.msg import JointState
from moveit_msgs.srv import GetPositionIK, GetPositionIKRequest
from std_srvs.srv import Empty
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from test_pkg_wenhan.srv import *

class test_ik():
    """docstring for test_fk"""
    def __init__(self):
        '''Initilize ros publisher, ros subscriber, ros service'''
        # Subscriber
        self.sub_jointState = rospy.Subscriber('/joint_states', JointState, self.cb_jointState)

        # Client
        self.client_ik = rospy.ServiceProxy('/lwr_kinematics/get_ik', GetPositionIK)
        self.client_jt = actionlib.SimpleActionClient('/lwrcontroller/joint_trajectory_action', FollowJointTrajectoryAction)

        # Declare a server
        self.server_setGripperLocation = rospy.Service('/set_gripper_location', SetGripperLocation, self.cb_setGripperLocation)


    def cb_jointState(self, msg):
      self.jointName = msg.name
      self.jointPosition = msg.position


    def cb_setGripperLocation(self, msg):
      # Calibration of A-axis
      msg.pose_stamped.pose.position.z = msg.pose_stamped.pose.position.z - 0.0075
      ik_request = GetPositionIKRequest()

      # Set the stamped pose of the link to be computed, dont forget the frame_id
      ik_request.ik_request.pose_stamped = msg.pose_stamped
      # Set the starting position(using current state information).
      ik_request.ik_request.robot_state.joint_state.name = self.jointName
      ik_request.ik_request.robot_state.joint_state.position = self.jointPosition
      
      ik_response = self.client_ik(ik_request)


      # Waits until the action server has started up and started listening for goals
      print 'Waiting for JTaction server...'
      self.client_jt.wait_for_server()
      print 'ok'

      # Creates a goal to send to the action server
      goal = FollowJointTrajectoryGoal()
      goal.trajectory.joint_names = ik_response.solution.joint_state.name

      joint_velocity = 0.08

      #base 1 (A1,A2-90,E1,A3,A4,A5,A6)
      goal.trajectory.points.append(JointTrajectoryPoint(ik_response.solution.joint_state.position, [joint_velocity]*7, [], [], rospy.Duration(40)));
      goal.trajectory.header.stamp = rospy.get_rostime() + rospy.Duration(0.001)

      # Sends the goal to the action server
      self.client_jt.send_goal(goal)

      # Waits for the server to finish performing the action
      self.client_jt.wait_for_result()

      return SetGripperLocationResponse()



if __name__ == '__main__':
    rospy.init_node('test_ik_node')
    rospy.loginfo("**test_ik_node initilized..")

    test_ik()

    rospy.spin()


















