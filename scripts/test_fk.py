#! /usr/bin/env python
# -*- coding: UTF-8 -*-


import roslib; roslib.load_manifest('lmtlwr')
import rospy

#from moveit_msgs.srv import GetPositionFK, GetPositionFKRequest, GetPositionFKResponse
from moveit_msgs.srv import GetPositionFK, GetPositionFKRequest
from sensor_msgs.msg import JointState

class test_fk():
    """docstring for test_fk"""
    def __init__(self):
        '''Initilize ros publisher, ros subscriber, ros service'''
        # Subscriber
        self.sub_jointState = rospy.Subscriber('/joint_states', JointState, self.cb_jointState)

        # Client
        self.client_fk = rospy.ServiceProxy('/lwr_kinematics/get_fk', GetPositionFK)
        rospy.sleep(1)

        fk_request = GetPositionFKRequest()
        fk_request.header.frame_id = 'base_link'

        # Link names for which forward kinematics must be computed
        fk_request.fk_link_names.extend(['lwr_arm_7_link'])
        # fk_request.fk_link_names.extend(['lwr_arm_1_link',
        #                                  'lwr_arm_2_link', 'lwr_arm_3_link',
        #                                  'lwr_arm_4_link', 'lwr_arm_5_link',
        #                                  'lwr_arm_6_link', 'lwr_arm_7_link'])

        # The current robot state, joint names and joint positions, to be used for forward kinematics
        fk_request.robot_state.joint_state.name = self.jointName
        fk_request.robot_state.joint_state.position = self.jointPosition

        #rospy.loginfo('Wait for service: /lwr_kinematics/get_fk ......')
        #rospy.wait_for_service('/lwr_kinematics/get_fk', 5.0)
        #rospy.loginfo('/lwr_kinematics/get_fk service waked up.')
        fk_response = self.client_fk(fk_request)

        fk_response.pose_stamped[0].pose.position.z += 0.0075
        #print len(fk_response.pose_stamped)
        #print "End-effector postition:\n", fk_response.pose_stamped[0].pose
        print "End-effector postition: ", fk_response

    def cb_jointState(self, msg):
      self.jointName = msg.name
      self.jointPosition = msg.position
        


if __name__ == '__main__':
    rospy.init_node('test_fk_node')
    rospy.loginfo("**test_fk_node initilized..")

    test_fk()

    #rospy.spin()


















