#! /usr/bin/env python
# -*- coding: UTF-8 -*-


import roslib; roslib.load_manifest('lmtlwr')
import rospy
import actionlib
import math

from control_msgs.msg import *
from trajectory_msgs.msg import *

def go_start():
    client = actionlib.SimpleActionClient('/lwrcontroller/joint_trajectory_action', FollowJointTrajectoryAction)

    # Waits until the action server has started up and started listening for goals
    print 'Waiting for JTaction server...'
    client.wait_for_server()
    print 'ok'

    r = math.pi / 180

    # Creates a goal to send to the action server
    goal = FollowJointTrajectoryGoal()

    goal.trajectory.joint_names = ['lwr_arm_0_joint', 'lwr_arm_1_joint', 'lwr_arm_2_joint', 'lwr_arm_3_joint', 'lwr_arm_4_joint', 'lwr_arm_5_joint', 'lwr_arm_6_joint']

    # Start point
    joint_velocity = 0.2

    #base 1 (A1,A2-90,E1,A3,A4,A5,A6)
    goal.trajectory.points.append(JointTrajectoryPoint([-0.41655367612838745, 1.3168530464172363, -0.6378539204597473, -1.1116833686828613, -0.7219914197921753, -1.5338581800460815, -0.6272910237312317], [joint_velocity]*7, [], [], rospy.Duration(40)));

    goal.trajectory.header.stamp = rospy.get_rostime() + rospy.Duration(0.001)

    # Sends the goal to the action server
    client.send_goal(goal)

    # Waits for the server to finish performing the action
    client.wait_for_result()



if __name__ == '__main__':
    rospy.init_node('test_wenhan')
    go_start()

    print "finished..."
