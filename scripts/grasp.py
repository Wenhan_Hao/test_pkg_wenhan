#! /usr/bin/env python
# -*- coding: UTF-8 -*-


import roslib; roslib.load_manifest('lmtlwr')
import rospy

import numpy as np

from wsg_50_common.msg import *
from test_pkg_wenhan.srv import *

class grasp_object():
    """docstring for grasp"""
    def __init__(self):
        '''Initilize ros publisher, ros subscriber, ros service'''
        # subscribed Topic
        self.sub_gripper_status = rospy.Subscriber('/wsg_50_driver/status', Status, self.cb_gripper_status)
        
        # Topic where we publish
        self.pub_gripper_position = rospy.Publisher('/wsg_50_driver/goal_position', Cmd, queue_size=10)
        
        # Declare a service
        self.service = rospy.Service('/grasp_srv', Grasp, self.cb_grasp_srv)

        self.last_forces_left = [0.0]*100
        self.last_forces_right = [0.0]*100
        self.force = 10

        #rospy.loginfo('default gripper force: %s', self.force)


    def cb_gripper_status(self, msg):
        self.width = msg.width

        self.last_forces_left.pop(0)
        self.last_forces_left.append(msg.force_finger0)

        self.last_forces_right.pop(0)
        self.last_forces_right.append(msg.force_finger1)

        #rospy.loginfo('Variance_left: %s   Variance_right: %s', np.array(self.last_forces_left).var(), np.array(self.last_forces_right).var())

    def cb_grasp_srv(self, msg):

        while True:
            forces_local = self.last_forces_right
            force_var = np.array(forces_local).var()

            # Wait until gripper force doesn't change
            while force_var >= 0.05:
                forces_local = self.last_forces_right
                force_var = np.array(forces_local).var()
                #rospy.loginfo('variance: %s', force_var)
            else:
                self.force = np.array(forces_local).mean()
                rospy.loginfo('force: %s     variance: %s', self.force, force_var)


            force_error = msg.force - self.force
            if force_error <= 0.3:
                #print 'Wait a while......'
                #rospy.sleep(2)
                #continue
                break

            if self.force <=0.2:
                if force_error >= 10:
                    self.pub_gripper_position.publish(pos = self.width-2, speed = 5)
                    rospy.sleep(0.4)
                else:
                    self.pub_gripper_position.publish(pos = self.width-1, speed = 5)
                    rospy.sleep(0.2)
            else:
                if force_error > 10:
                    self.pub_gripper_position.publish(pos = self.width-1, speed = 5)
                    rospy.sleep(0.2)
                elif force_error >= 8:
                    self.pub_gripper_position.publish(pos = self.width-0.8, speed = 5)
                    rospy.sleep(0.2)
                elif force_error >=5:
                    self.pub_gripper_position.publish(pos = self.width-0.5, speed = 5)
                    rospy.sleep(0.2)
                elif force_error >=3:
                    self.pub_gripper_position.publish(pos = self.width-0.3, speed = 5)
                    rospy.sleep(0.2)
                else:
                    self.pub_gripper_position.publish(pos = self.width-0.1, speed = 5)
                    rospy.sleep(0.2)

        return GraspResponse()

        


if __name__ == '__main__':
    rospy.init_node('grasp_node')
    rospy.loginfo("**grasp_node initilized..")

    grasp_object()

    rospy.spin()


















